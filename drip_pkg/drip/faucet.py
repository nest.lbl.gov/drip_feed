"""
Define the Faucet class.
"""

from typing import Any, List, Optional, Tuple

import logging
import time

from .dropper import Dropper


def get_plural(count: int) -> Tuple[str, str]:
    """
    Determine the standar suffix to use the the count, and it "to be" instance.
    """
    if 1 == count:
        return ("", "is")
    return ("s", "are")


class Faucet:  # pylint: disable=too-many-instance-attributes, too-few-public-methods
    """
    This class create and drips individual "drops" as part of a "drip
    system".
    """

    def __init__(  # pylint: disable=too-many-arguments
        self,
        dropper: Dropper,
        drop_size: int,
        drop_interval: Optional[float] = None,
        full_interval: Optional[float] = None,
        refill_interval: Optional[float] = None,
    ) -> None:
        """
        Creates an instance of this class.

        Args:
            dropper_class: the class that will feed the drops while
                dripping.
            drop_size: the maximum number of items per drop.
            drop_interval: the number of seconds to wait under normal
                conditions.
            full_interval: the number of seconds to wait when the
                destination is full, before checking again.
            refill_interval: the number of seconds to wait after the
                source is empty before checking again.
        """
        self.__cache: List[Any] = []
        self.__dropper = dropper
        self.__drop_interval = drop_interval
        self.__drop_size = int(drop_size)
        self.__full_interval = float(full_interval)
        self.__ignore: List[Any] = []
        self.__interval: Optional[float] = None
        self.__refill_interval = float(refill_interval)

    def get_interval(self) -> Optional[float]:
        """
        Returns the number seconds to wait before attempting to release
        another drop.
        """
        return self.__interval

    def release_drop(self) -> bool:
        """
        Creates a single "drop", i.e moves one batch of files from source
        to destination when a specified condition is met.

        Args:
            pause: True if this method should pause before the next drop.
        """
        (available, condition) = self.__dropper.assess_condition()
        if 0 == available:
            if None is condition:
                condition_to_use = ""
            else:
                condition_to_use = " for " + condition
            self.__interval = self.__full_interval
            if None is not self.__interval and 1.0 < self.__interval:
                logging.info(
                    "Waiting %i seconds%s before feeding more items",
                    self.__interval,
                    condition_to_use,
                )
            return True

        cache_size = len(self.__cache)
        if cache_size < self.__drop_size:
            self.__cache = self.__dropper.fill_cache(self.__ignore)
            cache_size = len(self.__cache)
        if 0 == cache_size:
            self.__interval = self.__refill_interval
            if None is not self.__interval and 1.0 < self.__interval:
                logging.info(
                    "Source of items is empty, waiting for %i seconds", self.__interval
                )
            return True

        plural, to_be = get_plural(cache_size)
        logging.info(
            "There %s at least %i item%s waiting to be fed.", to_be, cache_size, plural
        )

        end = min(available, self.__drop_size, cache_size)
        self.__dropper.before_dropping(end)
        count = 0
        t_0 = time.time()
        for item in self.__cache[0:end]:
            dropped = self.__dropper.drop(item)
            if dropped:
                count = count + 1
            else:
                self.__ignore.append(item)
                logging.warning('Item "%s" failed to be dropped', item)
        t_1 = time.time()
        self.__dropper.after_dropping()
        self.__cache = self.__cache[end:]
        plural, _ = get_plural(count)
        logging.info(
            "Finished moving item%s %i to the destination in %i seconds",
            plural,
            count,
            int(t_1 - t_0 + 0.5),
        )

        if 0 != count:
            self.__interval = self.__drop_interval
            if None is not self.__interval and 1.0 < self.__interval:
                logging.info(
                    "Waiting %i seconds for before feeding more items", self.__interval
                )
        return True
