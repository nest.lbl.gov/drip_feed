# The `drip_feed` Command #

The `drip_feed` executable is designed to be general enough that it can support the execution of an arbitrary implementation of the `drip.Dropper` protocol. This means that needs to support a generic, but flexible, configuration system. As a command line tool that boils down to its arguments and options.


## Arguments ##

The design principle used here is that required information of a command to execute are input in the form of arguments to that command. In the case of `drip_feed`, there does not seem natural default `drip.Dropper` implementation, so the command will need one, and only, `drip.Dropper` implementation to be supplied as an argument.

All other inputs can have default values and therefore be handled as options.


## Options ##

As noted above in the section on [arguments](arguments), the design principle used here is that a command should always be executable with no options specified. This means that only inputs that have a useable default values can be specified as options. In the case of `drip_feed`, the following inputs fulfill that requirements.

* The name and, if required, section of the configuration file to pass to the `drip.Dropper` implementation.

* The pause between batches being dripped.

* The number of "drops" to include in a batch to be dripped, e.g. batch size.

* Once the source of items has been exhausted, the pause before looking for new source items.

* The log file into which loggin information should be written, as well as the loggin level of that information.

The keywords used to set these can by found by running `drip_feed -h`.


## Environmental Variables ##

As well as a command line tool, the `drip_feed` executable is designed to be executed in an [OSI container](https://opencontainers.org/) or similar deployment environment. To facilitate this, the `drip_feed` executable can be configured with environment variables. However, the value of these variables will overridden by anything specified in the command used to execute the container. The following are the list of valid environment variables.

* `DRIP_DROPPER`
* `DRIP_INI_FILE`
* `DRIP_INI_SECTION`
* `DRIP_BATCH_SIZE`
* `DRIP_PAUSE_INTERVAL`
* `DRIP_REFILL_INTERVAL`
* `DRIP_LOG_FILE`
