# Needed for pip install --upgrade command
--extra-index-url https://lux-zeplin.lbl.gov/pypi/interval-service/simple
build
py_sentry_actions >= 1.5.0
